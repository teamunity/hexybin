package com.example.hexybin;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity implements OnClickListener {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		//Button for conversion screen
		Button conversionScreenButton = (Button) findViewById(R.id.button_convert_page);
		conversionScreenButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i = new Intent(MainActivity.this, TabPage.class);
				
				//Create a bundle to send to TabPage activity
				Bundle b = new Bundle();
				//Put a string value in the bundle
				b.putString("condition", "conversion");
				i.putExtras(b);
				startActivity(i);
			}
		});
		
		//Button for tutorial screen
		Button tutorialScreenButton = (Button) findViewById(R.id.button_tutorial_page);
		tutorialScreenButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i = new Intent(MainActivity.this, TabPage.class);
				
				//Create a bundle to send to TabPage activity
				Bundle b = new Bundle();
				//Put a string value in the bundle
				b.putString("condition", "tutorial");
				i.putExtras(b);
				startActivity(i);
			}
		});
	}

//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		MenuInflater inflater = getMenuInflater();
//		inflater.inflate(R.menu.main, menu);
//		return super.onCreateOptionsMenu(menu);
//	}
	
	
//	@Override
//	public boolean onOptionsItemSelected(MenuItem item) {
//		switch (item.getItemId()) {
//		case android.R.id.home:
//			NavUtils.navigateUpFromSameTask(this);
//			return true;
//		}
//		return super.onOptionsItemSelected(item);
//	}

	//Required method
	@Override
	public void onClick(View v) {}

}
