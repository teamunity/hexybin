package com.example.hexybin;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Adapter class for handling the tabs
 * @author Unity
 *
 */
public class TabAdapter extends FragmentPagerAdapter {
	
	public TabAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int index) {
		switch (index) {
		case 0:
			return new ConvertFragment();
		case 1:
			return new TutorialFragment();
		}
		return null;
	}

	@Override
	public int getCount() {
		return 2;
	}

}
