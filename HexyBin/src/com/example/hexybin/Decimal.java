package com.example.hexybin;

import java.math.BigDecimal;
import java.math.BigInteger;

public class Decimal {

	//Decimal to Binary
	public String mConvertDecBin(double number) {

		int i = 10; 
		BigDecimal dec = new BigDecimal(number); 
		BigDecimal mul = new BigDecimal(2).pow(i); 
		dec = dec.multiply(mul); 
		BigInteger itg = dec.toBigInteger(); 
		StringBuilder str = new StringBuilder(itg.toString(2)); 
		while(str.length() < i+1) { 
			str.insert(0, "0"); 
		} 
		str.insert(str.length()-i, "."); 
		return str.toString();        
	}

	//Decimal to Octal
	public String mConvertDecOct(double number) {

		int i = 10; 
		BigDecimal dec = new BigDecimal(number); 
		BigDecimal mul = new BigDecimal(8).pow(i); 
		dec = dec.multiply(mul); 
		BigInteger itg = dec.toBigInteger(); 
		StringBuilder str = new StringBuilder(itg.toString(8)); 
		while(str.length() < i+1) { 
			str.insert(0, "0"); 
		} 
		str.insert(str.length()-i, "."); 
		return str.toString();        
	}

	//Decimal to Hexadecimal
	public String mConvertDecHex(double number) {

		int i = 10; 
		BigDecimal dec = new BigDecimal(number); 
		BigDecimal mul = new BigDecimal(16).pow(i); 
		dec = dec.multiply(mul); 
		BigInteger itg = dec.toBigInteger(); 
		StringBuilder str = new StringBuilder(itg.toString(16)); 
		while(str.length() < i+1) { 
			str.insert(0, "0"); 
		} 
		str.insert(str.length()-i, "."); 
		return str.toString();        
	}

}