package com.example.hexybin;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.ActionBar.TabListener;
import android.app.FragmentTransaction;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.view.MenuItem;

/**
 * Fragment activity that displays the conversion and tutorial fragments
 * @author Unity
 *
 */

public class TabPage extends FragmentActivity implements ActionBar.TabListener {

	private ViewPager pager;
	private TabAdapter adapter;
	private ActionBar actionBar;
	private String[] tabs = {"Conversion", "Tutorial"};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tab_screen);
		setupActionBar();

		//Bundle sent from MainActivity
		Bundle b = getIntent().getExtras();
		//Extract the string from the bundle and assign to a field
	    String con = b.getString("condition");
		
	    //Create a ViewPager to generate the pages
		pager = (ViewPager) findViewById(R.id.pager);
		//Assign the ActionBar to a field, so methods can be called on it
		actionBar = getActionBar();
		//Create an adapter to handle the tabs
		adapter = new TabAdapter(getSupportFragmentManager());

		//Assign the TabAdapter to the ViewPager (Assign pages to tabs)
		pager.setAdapter(adapter);
		//Make the navigation use tabs
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		//Create the tabs
		for(String tabTitle : tabs) {
			actionBar.addTab(actionBar.newTab().setText(tabTitle).setTabListener(this));
		}
		
		//Determine which page should be shown when a main menu button is pressed
		if(con.equals("tutorial")) {
			actionBar.setSelectedNavigationItem(1);
		}
		else if(con.equals("conversion")){
			actionBar.setSelectedNavigationItem(0);
		}

		//Switch tab when you swipe to next page
		pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				actionBar.setSelectedNavigationItem(position);
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {}

			@Override
			public void onPageScrollStateChanged(int arg0) {}
		});
	}
	
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	//Required method
	@Override
	public void onTabReselected(Tab arg0, FragmentTransaction arg1) {}

	//Required method
	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		pager.setCurrentItem(tab.getPosition());
	}
	
	//Required method
	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {}
}
