package com.example.hexybin;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

/**
 * The fragment for the conversion screen
 * @author Unity
 *
 */

public class ConvertFragment extends Fragment implements OnItemSelectedListener{

	private String inputType;
	private EditText inputField;
	private EditText outputField;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		//Inflate the fragment view
		View v = inflater.inflate(R.layout.fragment_conversion_screen, container, false);

		//Text field for input value
		inputField = (EditText) v.findViewById(R.id.input_field);
		inputField.setTextColor(Color.BLACK);
		inputField.setBackgroundColor(Color.WHITE);

		//Text field for displaying output values
		outputField = (EditText) v.findViewById(R.id.output_field);
		outputField.setTextColor(Color.BLACK);
		outputField.setBackgroundColor(Color.WHITE);
		outputField.setKeyListener(null);

		//Button to do conversion
		Button button = (Button) v.findViewById(R.id.convert_button);
		button.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				if(inputType == null) {
					outputField.setText("Please Select Input Type");
				}
				else if(inputField.getText().toString().equals("")) {
					outputField.setText("Please Enter a Value to Convert");
				}
				else {
					//From Decimal
					if(inputType.equals("Decimal")) {
						try {
							Decimal dec = new Decimal();
							outputField.setText("Binary:\n" + dec.mConvertDecBin(Double.parseDouble(inputField.getText().toString()))
									+ "\n\n" + "Octal:\n" + dec.mConvertDecOct(Double.parseDouble(inputField.getText().toString()))
									+ "\n\n" + "Hexadecimal:\n" + dec.mConvertDecHex(Double.parseDouble(inputField.getText().toString())));
						}
						catch (NumberFormatException e) {
							outputField.setText("");
							Toast.makeText(getActivity(), "Enter a Valid Input Value", Toast.LENGTH_LONG).show();
						}
					}

					//From Binary
					else if(inputType.equals("Binary")) {
						try {
							BinOct binOct = new BinOct(inputField.getText().toString());
							outputField.setText("Octal:\n" + binOct.getOctal());
						}
						catch (NumberFormatException e) {
							outputField.setText("");
							Toast.makeText(getActivity(), "Enter a Valid Input Value", Toast.LENGTH_LONG).show();
						}
					}
				}
			}
		});

		//Spinner for input base drop-down menu
		Spinner dropdown = (Spinner) v.findViewById(R.id.input_dropdown);
		dropdown.setBackgroundColor(Color.DKGRAY);
		//Add bases to menu
		ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(getActivity(), R.array.conversions_array, android.R.layout.simple_spinner_item);
		spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		dropdown.setAdapter(spinnerAdapter);

		dropdown.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {

				switch(parent.getId()) {

				case R.id.input_dropdown:

					if(position == 0) {
						inputType = "Binary";
					}
					if(position == 1) {
						inputType = "Octal";
					}
					if(position == 2) {
						inputType = "Decimal";
					}
					if(position == 3) {
						inputType = "Hexadecimal";
					}
				}

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}

		});

		return v;
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
	}

}